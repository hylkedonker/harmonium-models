{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import logging\n",
    "from tempfile import mkdtemp\n",
    "\n",
    "from lifelines import CoxPHFitter, KaplanMeierFitter\n",
    "from lifelines.datasets import load_rossi\n",
    "from lifelines.utils.sklearn_adapter import sklearn_adapter\n",
    "\n",
    "from matplotlib import pyplot as plt\n",
    "import pandas as pd\n",
    "from scipy.stats import uniform\n",
    "from sklearn.model_selection import RandomizedSearchCV, train_test_split\n",
    "from sklearn.preprocessing import StandardScaler\n",
    "from sklearn.utils.fixes import loguniform\n",
    "\n",
    "from harmoniums import SurvivalHarmonium\n",
    "from harmoniums.utils import reset_random_state\n",
    "from harmoniums.views import plot"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "logging.getLogger().setLevel(logging.INFO)\n",
    "reset_random_state(1234)\n",
    "# Choose compute budget. \n",
    "# This is the amount of hyperparameter configurations to test.\n",
    "n_iter = 25"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lifelines example dataset\n",
    "Dataset of convicts released from the Maryland state prisons: https://lifelines.readthedocs.io/en/latest/lifelines.datasets.html#lifelines.datasets.load_rossi\n",
    "where the event is the time until the arrest of a convict."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = load_rossi()\n",
    "X.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The dataset consists of four types of variables:\n",
    "- Categorical variables: `fin`, `race`, `wexp`, `mar`, `paro`\n",
    "- Numeric variable: `age`\n",
    "- Ordinal variable: `prio`\n",
    "- Time-to-event variable `week` (with event indicator `arrest`).\n",
    "\n",
    "The ordinal variable `prio` indicates the number of convictions prior to current incarceration."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X['prio'].value_counts()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Because there are limited number of convicts with more than 5 prior convictions, lets group these together and encode the variable using one-hot-encoding."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Make dummy categories for the number of prior convictions.\n",
    "prior_dummies = ['prio_0', 'prio_1', 'prio_2', 'prio_3', 'prio_4', 'prio_5', 'prio>5']\n",
    "X[prior_dummies] = pd.get_dummies(X['prio'].apply(lambda x: '>5' if x > 5 else x ))\n",
    "X = X.drop('prio', axis=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Putting this all together, we have the following set of variables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "categorical_columns = ['fin', 'race', 'wexp', 'mar', 'paro'] + prior_dummies\n",
    "numeric_columns = ['age']\n",
    "survival_columns = ['week']\n",
    "event_columns = ['arrest']\n",
    "\n",
    "# Standardise the numeric variable `age`.\n",
    "X[numeric_columns] = StandardScaler().fit_transform(X[numeric_columns])\n",
    "X.describe()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_train, X_test = train_test_split(X)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Kaplan-Meier"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "kmf = KaplanMeierFitter()\n",
    "kmf.fit(durations=X['week'], event_observed=X['arrest']).plot()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Cox regression\n",
    "Use cross validation to find optimal $L_2$ penalty."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "CoxRegression = sklearn_adapter(CoxPHFitter, event_col='arrest')\n",
    "# CoxRegression is a class like the `LinearRegression` class or `SVC` class in scikit-learn\n",
    "\n",
    "cph = CoxRegression()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cph_cv = RandomizedSearchCV(\n",
    "    cph, \n",
    "    param_distributions={\n",
    "    \"penalizer\": loguniform(1e-5, 1e2),\n",
    "    \"l1_ratio\": loguniform(1e-5, 1),\n",
    "    }, \n",
    "    cv=5,\n",
    "    n_iter=n_iter,\n",
    "    n_jobs=1,\n",
    ").fit(X_train.drop('week', axis=1), X_train['week'])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cph = cph_cv.best_estimator_\n",
    "print(cph)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Accuracy\n",
    "Compute Harrell's concordance index (c-index) on the test set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cph.score(X_test.drop('week', axis=1), X_test['week'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Harmonium\n",
    "In view of the larger number of hyperparameters we will use a randomised grid search (as opposed to exhaustive search for the Cox model). The models are scored using the `score` function, which is just the concordance index."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Time horizon of the model (model events in the range [0, max_t]).\n",
    "max_t = X['week'].max(axis=0)\n",
    "\n",
    "log_dir = mkdtemp()\n",
    "harmonium = SurvivalHarmonium(\n",
    "    categorical_columns=categorical_columns,\n",
    "    survival_columns=survival_columns,\n",
    "    numeric_columns=numeric_columns,\n",
    "    event_columns=event_columns,\n",
    "    verbose=True,\n",
    "    log_every_n_iterations=300, \n",
    "    time_horizon=[max_t],\n",
    "    # Don't use median value because this value \n",
    "    # is not observed in the dataset (see Kaplan-Meier).\n",
    "    risk_score_time_point=0.75 * max_t,\n",
    "    # Don't evaluate any metrics for now.\n",
    "    metrics=('log_likelihood', 'score', 'brier_loss'),\n",
    "    X_validation=X_test,\n",
    "    # Log to temporary directory.\n",
    "    output=log_dir,\n",
    "    CD_steps=1,\n",
    "    n_hidden_units=2,\n",
    "    persistent=True,\n",
    ")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f'To follow training, run:\\ntensorboard --logdir {log_dir}')\n",
    "harm_cv = RandomizedSearchCV(\n",
    "    harmonium, \n",
    "    param_distributions={\n",
    "        \"learning_rate\": loguniform(1e-5, 0.1),\n",
    "        \"n_epochs\": loguniform(1e1, 2e4),\n",
    "        \"momentum_fraction\": uniform(0, 0.9),\n",
    "        \"mini_batch_size\": loguniform(25, 1000),\n",
    "        \"weight_decay\": loguniform(1e-5, 0.1),\n",
    "        \"persistent\": [True, False],\n",
    "    },\n",
    "    cv=5,\n",
    "    n_iter=n_iter,\n",
    "    n_jobs=-1,\n",
    "    refit=True,\n",
    ").fit(X_train)\n",
    "print(harm_cv.best_params_)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "harmonium = harm_cv.best_estimator_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "By looking at the weights of the model, we can see what variables contribute to the selection (i.e., activation) of the latent state."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "harm_cv.best_params_"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot(harmonium)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Training progress"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "score_train = harmonium.get_train_metrics()\n",
    "score_val = harmonium.get_validation_metrics()\n",
    "\n",
    "plt.subplot(1,2,1)\n",
    "plt.plot(score_train['brier_loss'], label='train')\n",
    "plt.plot(score_val['brier_loss'], label='test')\n",
    "plt.ylabel('Brier loss')\n",
    "plt.legend(frameon=False)\n",
    "\n",
    "\n",
    "plt.subplot(1,2,2)\n",
    "plt.plot(score_train['score'], label='train')\n",
    "plt.plot(score_val['score'], label='test')\n",
    "plt.ylabel(\"Harrell's concordance index\")\n",
    "plt.legend(frameon=False)\n",
    "plt.tight_layout()\n",
    "\n",
    "plt.figure()\n",
    "plt.plot(score_train['log_likelihood'], label='train')\n",
    "plt.plot(score_val['log_likelihood'], label='test')\n",
    "plt.ylabel('log likelihood')\n",
    "plt.legend(frameon=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Accuracy\n",
    "What is the concordance on the test set?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "harmonium.score(X_test)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
